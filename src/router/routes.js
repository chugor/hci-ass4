
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'timekeeping', component: () => import('pages/TimeKeeping.vue') },
      { path: 'timetelling', component: () => import('pages/TimeTelling.vue') },
      { path: 'multiplication', component: () => import('pages/Multiplication.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
